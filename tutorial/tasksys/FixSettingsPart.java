package scripts.tutorial.tasksys;

import org.tbot.methods.*;
import org.tbot.wrappers.WidgetChild;
import scripts.tutorial.STSettings;
import scripts.tutorial.SellTutorial;

import java.awt.*;
import java.util.Properties;

public class FixSettingsPart extends ScriptPart {

	public FixSettingsPart() {
		Properties gameProps = SellTutorial.propsManager.getPropertiesFile(STSettings.GAME_SETTINGS);
		zoomOut = Boolean.parseBoolean(STSettings.getPropertyFromPropsFile("zoomOut",gameProps));
		musicVolume = Integer.parseInt(STSettings.getPropertyFromPropsFile("musicVolume",gameProps));
		soundVolume = Integer.parseInt(STSettings.getPropertyFromPropsFile("soundVolume",gameProps));
		areaVolume = Integer.parseInt(STSettings.getPropertyFromPropsFile("areaVolume",gameProps));
		brightness = Integer.parseInt(STSettings.getPropertyFromPropsFile("brightness",gameProps));
		clickType = Integer.parseInt(STSettings.getPropertyFromPropsFile("clickType",gameProps));
		if(clickType != 0) {
			needToOpenControl = true;
			setClickType = true;
		}
		SellTutorial.propsManager.savePropertiesFile(STSettings.GAME_SETTINGS,gameProps);
	}

	@Override
	public int run() {

		// Skip Part
		if(Settings.get(281) <= 3) {
			return 0;
		}

		if(roofSet) {
			// Close Adv Options
			WidgetChild button = Widgets.getWidget(60,2);
			if(button != null && button.isVisible()) {
				button = button.getChild(11);
				// Check if roof is really off
				WidgetChild roofButton = Widgets.getWidget(60,8);
				if(roofButton != null && roofButton.isVisible()) {
					if(roofButton.getTextureID() != 762) {
						roofSet = false;
					} else {
						button.click(); // close window
					}
				}

			} else if(soundSet[0] && soundSet[1] && soundSet[2] && !setClickType && Settings.get(166) == brightness) {
				// Done Changing Settings
				return -1;

			} else {
				// Open Sound Tab
				if(needToOpenSound) {
					WidgetChild soundTab = Widgets.getWidget(261,1);
					if(soundTab != null && soundTab.isVisible()) {
						soundTab = soundTab.getChild(2);
						if(soundTab != null && soundTab.isVisible()) {
							soundTab.click();
							if(needToOpenSound) {
								out("needToOpenSound = false");
								needToOpenSound = false;
							}
						}
					}
				} else {
					for (int i = 0; i < soundSet.length; i++) {
						if(!soundSet[i]) {
							toggleSound(i);
						}
					}
				}
			}

		} else if(Widgets.isTabOpen(Widgets.TAB_OPTIONS)) {

			// Open Display Tab
			if(needToOpenDisplay) {
				WidgetChild displayTab = Widgets.getWidget(261,1);
				if(displayTab != null && displayTab.isVisible()) {
					displayTab = displayTab.getChild(0);
					if(displayTab != null && displayTab.isVisible()) {
						displayTab.click();
						if(needToOpenDisplay) {
							out("needToOpenDisplay = false");
							needToOpenDisplay = false;
						}
					}
				}
			} else

			if(Settings.get(166) != brightness) {
				WidgetChild brightWidget = Widgets.getWidget(261,14 + brightness);
				if(brightWidget != null && brightWidget.isVisible()) {
					brightWidget.click();
					out("Set Brightness");
				}
			} else

			// Fix Zoom
			if(!zoomSet && zoomOut) {
				WidgetChild zoomButton = Widgets.getWidget(261,13);
				if(zoomButton != null && zoomButton.isVisible()) {
					if(needToOpenDisplay) {
						out("needToOpenDisplay = false");
						needToOpenDisplay = false;
					}
					if(zoomButton.getX() > 601) {
						if(zoomButton.getBounds().contains(Mouse.getLocation())) {
							if(!Mouse.isPressed()) {
								Mouse.press(Mouse.getX(),Mouse.getY(),true);
							} else {
								Mouse.move(new Point(Random.nextInt(250,550),Random.nextInt(250,450)));
							}
						} else {
							Mouse.move(new Point(zoomButton.getX() + Random.nextInt(zoomButton.getWidth()),
									zoomButton.getY() + Random.nextInt(zoomButton.getHeight())));
						}
					} else {
						out("zoomSet = true");
						zoomSet = true;
						if(Mouse.isPressed()) {
							Mouse.release(Mouse.getX(),Mouse.getY(),true);
						}
					}
				} else {
					out("needToOpenDisplay = true");
					needToOpenDisplay = true;
				}
			} else

			// Raise Camera
			if(Camera.getPitch() < 98 /*&& !Camera.isLocked()*/) {
				out("Raise Camera");
				Camera.setPitch(98);
			} else

			// Toggle Roof
			if(!roofSet) {
				WidgetChild advOptionsWindow = Widgets.getWidget(60,8);
				if(advOptionsWindow != null && advOptionsWindow.isVisible()) {
					// Toggle Roof
					out("Toggle Roof Button Texture: " + advOptionsWindow.getTextureID());
					if(advOptionsWindow.getTextureID() == 762) {
						out("roofSet = true;");
						roofSet = true;
					} else {
						advOptionsWindow.click();
					}
				} else {
					// Open Adv Options
					WidgetChild advOptions = Widgets.getWidget(261,21);
					if(advOptions != null && advOptions.isVisible()) {
						if(needToOpenDisplay) {
							out("needToOpenDisplay = false");
							needToOpenDisplay = false;
						}
						advOptions = advOptions.getChild(9);
						if(advOptions != null && advOptions.isVisible()) {
							advOptions.click();
						}
					} else {
						out("needToOpenDisplay = true");
						needToOpenDisplay = true;
					}
				}
			}

			// Open Display Tab
			if(needToOpenControl) {
				WidgetChild controlTab = Widgets.getWidget(261,1);
				if(controlTab != null && controlTab.isVisible()) {
					controlTab = controlTab.getChild(6);
					if(controlTab != null && controlTab.isVisible()) {
						controlTab.click();
						if(needToOpenControl) {
							out("needToOpenDisplay = false");
							needToOpenControl = false;
						}
					}
				}
			} else

			if(clickType == 1 && Settings.get(1107) != 1) {
				WidgetChild alwaysRightClick = Widgets.getWidget(261,60);
				if(alwaysRightClick != null && alwaysRightClick.isVisible()) {
					alwaysRightClick.click();
					out("Click Control set to Always Right-Click.");
					setClickType = false;
				}
			} else if(clickType == 2 && Settings.get(1107) != 2) {
				WidgetChild leftClick = Widgets.getWidget(261,59);
				if(leftClick != null && leftClick.isVisible()) {
					leftClick.click();
					out("Click Control set to Left-Click When Available.");
					setClickType = false;
				}
			}

		} else {
			Widgets.openTab(Widgets.TAB_OPTIONS);
		}

		return Random.nextInt(450,850);
	}

	private void toggleSound(int index) {
		int volIncrease = 0;
		if(index == 0) {
			volIncrease = musicVolume;
		} else if(index == 1) {
			volIncrease = soundVolume;
		} else if(index == 2) {
			volIncrease = areaVolume;
		}
		WidgetChild soundOption = Widgets.getWidget(261,24 + (index * 6) + volIncrease);
		if(soundOption != null && soundOption.isVisible()) {
			if(soundOption.getTextureID() < 690) {
				System.out.println("soundSet[" + index + "] = true");
				soundSet[index] = true;
			} else {
				soundOption.click();
			}
		} else {
			out("needToOpenSound = true");
			needToOpenSound = true;
		}
	}

	@Override
	public String getName() {
		return "Settings";
	}

	@Override
	public String getPrefix() {
		return "SET";
	}

	private boolean zoomSet = false;
	private boolean roofSet = false;
	private boolean needToOpenDisplay = false;

	private boolean needToOpenControl = false;
	private boolean setClickType = false;

	private boolean soundSet[] = new boolean[] {false,false,false};
	private boolean needToOpenSound = false;

	// *** Game Settings *** //
	private boolean zoomOut = true; // default true
	private int musicVolume = 0; // 0-4 default 0
	private int soundVolume = 0; // 0-4 default 0
	private int areaVolume = 0; // 0-4 default 0
	private int brightness = 0; // 1-4 default 2
	private int clickType = 0; // 1-3 default 1
}
