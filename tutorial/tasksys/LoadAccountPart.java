package scripts.tutorial.tasksys;

import org.tbot.bot.Account;
import org.tbot.bot.TBot;
import org.tbot.methods.Skills;
import scripts.tutorial.STSettings;
import scripts.tutorial.SellTutorial;
import scripts.tutorial.methods.creation.ListReader;

import java.util.Properties;

public class LoadAccountPart extends ScriptPart {

	@Override
	public int run() {
		Properties props = SellTutorial.propsManager.getPropertiesFile(STSettings.GENERAL_SETTINGS);
		if(Boolean.parseBoolean(STSettings.getPropertyFromPropsFile("createAccounts",props))) {
			SellTutorial.propsManager.savePropertiesFile(STSettings.GENERAL_SETTINGS,props);
			return -1;
		} else {
			SellTutorial.propsManager.savePropertiesFile(STSettings.GENERAL_SETTINGS,props);
			String accountDetails = ListReader.getLoginDetails();
			if(accountDetails == null) {
				out(true,"No more accounts in premade_accounts.txt file.");
			} else if(accountDetails.contains(":")) {
				String[] splitDetails = accountDetails.split(":");
				if(splitDetails.length == 2) {
					String username = splitDetails[0];
					String password = splitDetails[1];
					Account setAccount = new Account(username,password,"", Skills.Skill.MINING);
					TBot.getBot().setCurrentAccount(setAccount);
					System.out.println("Loaded Account: " + username);
					return -1;
				} else {
					out(true,"Improper formatting in premade accounts file.");
					out(true,"Use Format As Follows: EMAIL:PASSWORD");
				}
			}
		}
		out(true,"Failed.");
		return -1;
	}

	@Override
	public String getName() {
		return "Account Loader";
	}

	@Override
	public String getPrefix() {
		return "LOAD";
	}
}
