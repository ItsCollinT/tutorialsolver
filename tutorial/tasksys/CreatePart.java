package scripts.tutorial.tasksys;

import api.ContentWriter;
import org.tbot.bot.Account;
import org.tbot.bot.TBot;
import org.tbot.methods.Random;
import org.tbot.methods.Skills;
import scripts.tutorial.STSettings;
import scripts.tutorial.SellTutorial;
import scripts.tutorial.methods.creation.AccountCreationTask;
import scripts.tutorial.methods.creation.BaseType;
import scripts.tutorial.methods.creation.ListReader;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.Properties;

public class CreatePart extends ScriptPart {

	public CreatePart() {
		Properties fileProps = SellTutorial.propsManager.getPropertiesFile(STSettings.CREATION_SETTINGS);
		minAge = Integer.parseInt(STSettings.getPropertyFromPropsFile("minAge",fileProps));
		maxAge = Integer.parseInt(STSettings.getPropertyFromPropsFile("maxAge",fileProps));
		baseEmail = STSettings.getPropertyFromPropsFile("baseEmail",fileProps);
		staticName = STSettings.getPropertyFromPropsFile("staticName",fileProps);
		baseType = BaseType.values()[Integer.parseInt(STSettings.getPropertyFromPropsFile("nameType",fileProps))];
		baseTypeEmail = BaseType.values()[Integer.parseInt(STSettings.getPropertyFromPropsFile("emailType",fileProps))];
		defDomain = STSettings.getPropertyFromPropsFile("defaultDomain",fileProps);
		minLength = Integer.parseInt(STSettings.getPropertyFromPropsFile("passMinLen",fileProps));
		maxLength = Integer.parseInt(STSettings.getPropertyFromPropsFile("passMaxLen",fileProps));
		staticPass = STSettings.getPropertyFromPropsFile("staticPass",fileProps);
		useStatic = Boolean.parseBoolean(STSettings.getPropertyFromPropsFile("useStatic",fileProps));
		useAlpha = Boolean.parseBoolean(STSettings.getPropertyFromPropsFile("useAlpha",fileProps));
		useDigits = Boolean.parseBoolean(STSettings.getPropertyFromPropsFile("useDigits",fileProps));
		useCaps = Boolean.parseBoolean(STSettings.getPropertyFromPropsFile("useCaps",fileProps));
		SellTutorial.propsManager.savePropertiesFile(STSettings.CREATION_SETTINGS,fileProps);
	}

	@Override
	public int run() {
		if(TBot.getBot().getCurrentAccount().getUsername().equals("CREATE")) {
			if(create()) {
				return -1;
			}
			return 10000;
		}
		TBot.getBot().getScriptHandler().getRandomHandler().getLoadedRandom("Auto Login").enable();
		return -1;
	}

	@Override
	public String getName() {
		return "Creation";
	}

	@Override
	public String getPrefix() {
		return "CRT";
	}

	private boolean create() {

		out("Attempting to create account...");
		try {
			AccountCreationTask task = new AccountCreationTask(generateUsername(),generateEmail(),generatePassword(),
					Random.nextInt(minAge,maxAge));
			int result = task.execute();
			out(task.toString());
			if(result == 0) {
				out(true,"SUCCESS! Created Account " + task.getDisplayName() + " [" + task.getEmail() + "]");
				Account account = new Account(task.getEmail(),task.getPass(),"", Skills.Skill.MINING);
				STSettings.accEmail = task.getEmail();
				TBot.getBot().setCurrentAccount(account);
				TBot.getBot().getScriptHandler().getRandomHandler().getLoadedRandom("Auto Login").enable();
				String timeStamp = "" + new Timestamp((new java.util.Date()).getTime());
				ContentWriter write = new ContentWriter(TBot.getBot().getScriptHandler().getScript());
				write.writeToFileWithHeader(
						"created_accounts",
						"### ~ Wow! Tutorial Pro by Collin ~ ###",
						task.getDisplayName() + "\t" + task.getEmail() + "\t" + task.getPass() + "\t" +
							STSettings.ipAddress + "\t" + timeStamp
				);
				return true;
			} else if(result == 1) {
				out(true,"Username [" + task.getDisplayName() + "] has been taken.");
			} else if(result == 2) {
				out(true,"Email address [" + task.getEmail() + "] has been taken.");
			} else if(result == 3) {
				out(true, "Username [" + task.getDisplayName() + "] has bad words.");
			} else if(result == 4) {
				out(true, "You have been blocked from creationg accounts. Too many created recently.");
			} else if(result == 5) {
				out(true, "Not eligible for account. Age issue?");
			} else {
				out(true,"Unknown problem, possibly connection related.");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private String generateEmail() {
		String result = "";
		if(baseTypeEmail.equals(BaseType.STATIC)) {
			result = baseEmail;
			result = replaceSymbols(result);
		} else if(baseTypeEmail.equals(BaseType.WORD_LIST)) {
			result = ListReader.getEmailPrefix();
			if(!result.contains("@")) {
				result += defDomain;
			}
		}

		return result;
	}

	private String generateUsername() {
		String result = "";
		if(baseType.equals(BaseType.STATIC)) {
			result = staticName;
			result = replaceSymbols(result);
		} else if(baseType.equals(BaseType.WORD_LIST)) {
			result = ListReader.getUsername();
		} else if(baseType.equals(BaseType.HARVEST)) {
			//TODO
		}

		return result;
	}

	private String replaceSymbols(String in) {
		while(in.contains("#")) {
			in = in.replaceFirst("#", "" + Random.nextInt(10));
		}
		while(in.contains("$")) {
			in = in.replaceFirst("\\$", "" + alpha.charAt(Random.nextInt(alpha.length() - 1)));
		}
		while(in.contains("?")) {
			in = in.replaceFirst("\\?", (Random.nextBoolean() ? "" + Random.nextInt(10)
					: "" + alpha.charAt(Random.nextInt(alpha.length() - 1))));
		}
		return in;
	}

	private String generatePassword() {
		if(useStatic) {
			return staticPass;
		}

		String result = "";

		while(result.length() <= Random.nextInt(minLength,maxLength)) {
			switch(Random.nextInt(2)) {
				case 0: {
					if(useAlpha) {
						result += (useCaps && Random.nextBoolean() ?
								alpha.toUpperCase().charAt(Random.nextInt(alpha.length()-1)) :
								alpha.charAt(Random.nextInt(alpha.length()-1)));
					}
				} break;
				case 1: {
					if(useDigits) {
						result += "" + Random.nextInt(10);
					}
				} break;
			}
		}

		return result;
	}

	// *** Creation Settings *** //
	// Account
	private int minAge = 16;
	private int maxAge = 25;

	// Email
	private String baseEmail = "def??????@co.com";
	private String defDomain = "@hotmail.com";
	private BaseType baseTypeEmail = BaseType.STATIC;

	// Username
	private String staticName = "ch3ck?????";
	private final String alpha = "abcdefghijklmnopqrstuvwxyz";
	private BaseType baseType = BaseType.STATIC;

	// Password
	private int minLength = 6;
	private int maxLength = 12;
	private String staticPass = "DefaultPass";
	private boolean useStatic = false;
	private boolean useAlpha = true;
	private boolean useDigits = true;
	private boolean useCaps = true;

}
