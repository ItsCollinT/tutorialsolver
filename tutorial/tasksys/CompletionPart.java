package scripts.tutorial.tasksys;

import api.CBankMethods;
import api.CWalking;
import org.tbot.internal.handlers.LogHandler;
import org.tbot.methods.Bank;
import org.tbot.methods.Random;
import org.tbot.methods.tabs.Inventory;
import org.tbot.wrappers.Tile;
import scripts.tutorial.STSettings;
import scripts.tutorial.SellTutorial;

import java.util.Properties;

public class CompletionPart extends ScriptPart {

	public CompletionPart() {
		Properties props = SellTutorial.propsManager.getPropertiesFile(STSettings.GENERAL_SETTINGS);
		String tileStr = STSettings.getPropertyFromPropsFile("walkTo",props);
		try {
			String[] coords = tileStr.split(",");
			if (coords.length == 3) {
				walkTo = new Tile(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2]));
			} else if (coords.length == 2) {
				walkTo = new Tile(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LogHandler.log("walkTo field formatting error. Format as follows: X,Y,Z");
		}

		try {
			bankFirst = Boolean.parseBoolean(STSettings.getPropertyFromPropsFile("bankItemsFirst",props));
			String itemIDStr = STSettings.getPropertyFromPropsFile("dontBankIDs",props);
			if (itemIDStr.contains(",")) {
				String[] idStrs = itemIDStr.split(",");
				exceptIDs = new int[idStrs.length];
				for (int i = 0; i < idStrs.length; i++) {
					exceptIDs[i] = Integer.parseInt(idStrs[i]);
				}
			} else {
				exceptIDs = new int[]{Integer.parseInt(itemIDStr)};
			}
		} catch (NumberFormatException e) {
			LogHandler.log("Error parsing item IDs. Format as follows: 1335,1337,1339");
			LogHandler.log("Only want one item? Simply list the item ID and nothing else, no commas.");
		}
	}

	@Override
	public int run() {
		if(walkTo == null) {
			return -1;
		} else if(bankFirst && Inventory.getCountExcept(exceptIDs) > 0) {
			if(Bank.isOpen()) {
				Bank.depositAllExcept(exceptIDs);
			} else {
				CBankMethods.openNearestBank();
			}
		} else if(walkTo.distance() > 4) {
			CWalking.getCWalking().traverse(walkTo);
		} else if(STSettings.terminateTopbot) {
			System.out.println("TERMINATING TOPBOT PROCESS");
			System.exit(0);
			return -1;
		} else {
			return -1;
		}

		return Random.nextInt(450, 950);
	}

	@Override
	public String getName() {
		return "Completion";
	}

	@Override
	public String getPrefix() {
		return "FIN";
	}

	Tile walkTo = null;
	boolean bankFirst = false;
	int[] exceptIDs = new int[] { };
}
