package scripts.tutorial.tasksys;

import org.tbot.methods.Camera;
import org.tbot.methods.Game;
import org.tbot.methods.Random;
import org.tbot.methods.Widgets;
import scripts.tutorial.methods.TutorialThings;

public class IslandPart extends ScriptPart {

	@Override
	public int run() {

		if(Game.isLoggedIn()) {

			// Continue
			if(Widgets.canContinue()) {
				System.out.println("Continue...");
				org.tbot.methods.input.keyboard.Keyboard.pressKey(' ');
				return Random.nextInt(450, 650);
			}

			// Camera
			if(Camera.getPitch() < 98/* && !Camera.isLocked()*/) {
				System.out.println("Raising camera...");
				Camera.setPitch(98);
				return Random.nextInt(450, 650);
			}

			// Tutorial!
			return TutorialThings.complete();
		}

		return 1000;
	}

	@Override
	public String getName() {
		return "Island";
	}

	@Override
	public String getPrefix() {
		return "TUT";
	}

}
