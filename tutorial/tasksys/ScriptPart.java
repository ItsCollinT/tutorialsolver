package scripts.tutorial.tasksys;

import api.PrintingObject;

public abstract class ScriptPart extends PrintingObject {

	public ScriptPart() {
		System.out.println("Loaded Part: " + getName());
	}

	public abstract int run();
	public abstract String getName();

}
