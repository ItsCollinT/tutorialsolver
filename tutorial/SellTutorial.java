package scripts.tutorial;

import api.MiscMethods;
import api.PropertyManager;
import org.tbot.bot.TBot;
import org.tbot.internal.AbstractScript;
import org.tbot.internal.Manifest;
import org.tbot.internal.ScriptCategory;
import org.tbot.internal.event.listeners.PaintListener;
import scripts.tutorial.tasksys.*;

import java.awt.*;
import java.util.LinkedList;
import java.util.Queue;

import static scripts.tutorial.STSettings.*;

@Manifest(name = "Wow Tutorial Pro", authors = "Collin", version = 0.34, description = "View thread for setup instructions", category = ScriptCategory.OTHER)
public class SellTutorial extends AbstractScript implements PaintListener {

	/*
	=======Future Features (X = done, - = not done)
	X Load names and emails from text files
	X Add 'walk to location' for bots
	- Add option to train certain skills to 3 before moving on

	=======Bugs

	Sloppy
	- Clicks book shelf instead of ladder
	- Attacks rats that are in combat

	 */


	// *** Public Static Stuff *** //
	public static PropertyManager propsManager;

	// *** Big Things *** //
	private Queue<ScriptPart> scriptParts = new LinkedList<>();
	private ScriptPart currentPart = null;
	private ScriptPart tempActivePart = null;
	private STPaint painter;

	// *** Settings *** //

	@Override
	public boolean onStart() {
		System.out.println("Starting Script");

		// Disable Randoms
		System.out.println("Disabling Randoms...");
		TBot.getBot().getScriptHandler().getRandomHandler().disableAll();

		// Load Settings
		System.out.println("Loading Config Files...");
		propsManager = new PropertyManager(this,
				CREATION_SETTINGS,GENERAL_SETTINGS,GAME_SETTINGS
		);
		String a;
		while((a = propsManager.getFileToCreate()) != null) {
			if(createPropertyFile(a)) {
				System.out.println("Created Config File for: " + a);
			}
		}

		// Load Script Settings
		loadScriptSettings();
		fillDefaultValues();

		// Load Script Parts
		System.out.println("Loading Parts...");
		scriptParts.add(new LoadAccountPart());
		scriptParts.add(new CreatePart());
		scriptParts.add(new FixSettingsPart());
		scriptParts.add(new IslandPart());
		scriptParts.add(new CompletionPart());

		// Check IP
		System.out.println("Checking IP...");
		try {
			ipAddress = MiscMethods.getIp();
		} catch (Exception e) {
			ipAddress = "IP_CHECK_FAIL";
		}
		System.out.println("Obtained IP: " + ipAddress);

		// Setup Paint
		System.out.println("Setting up paint...");
		painter = new STPaint(this);
		painter.setIpAddress(ipAddress);
		painter.setMouseColor(Color.GREEN);

		System.out.println("Done with setup, starting script...");
		return true;
	}

	@Override
	public int loop() {

		if(tempActivePart != null) {
			System.out.println("Temporary Active Part: " + tempActivePart.getName());
			tempActivePart.run();
			tempActivePart = null;

		} else if(currentPart != null) {
			int result = currentPart.run();
			if(result == -1) {
				System.out.println("Removing Part: " + currentPart.getName());
				currentPart = null;
			} else if(result == 0) {
				if(scriptParts.isEmpty()) {
					log("Script Error: Cannot Skip Active Part, List Empty. [" + currentPart.getName() + "]");
					log("Report this error to Collin via PM or thread comment.");
					return -1;
				}
				System.out.println("Skipping Part: " + currentPart.getName());
				tempActivePart = scriptParts.peek();
				return 100;
			} else {
				return result;
			}

		} else {
			currentPart = scriptParts.poll();
			if(currentPart != null) {
				System.out.println("Active Part Set To " + currentPart.getName());
			} else {
				System.out.println("No more parts, script finished.");
				return -1;
			}

		}

		return 500;
	}

	@Override
	public void onRepaint(Graphics g) {
		if(painter != null) {
			painter.doGraphics(g);
		}
	}
}
