package scripts.tutorial;

import api.SPainter;
import org.tbot.internal.AbstractScript;

import java.awt.*;

public class STPaint extends SPainter {

	public STPaint(AbstractScript script) {
		super(script);
	}

	@Override
	public void paint(Graphics g) {
		drawText(g,"Account: " + STSettings.accEmail);
	}

}
