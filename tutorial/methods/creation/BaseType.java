package scripts.tutorial.methods.creation;

public enum BaseType {

	WORD_LIST(0), // Loads list of names, substitutes characters
	STATIC(1), // Uses single string, substitutes characters
	HARVEST(2) // Scrapes highscores, switches numbers
	;

	BaseType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	int id;
}
