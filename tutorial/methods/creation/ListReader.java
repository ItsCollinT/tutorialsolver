package scripts.tutorial.methods.creation;

import org.tbot.bot.TBot;
import org.tbot.internal.handlers.LogHandler;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

public class ListReader {

	public static String getUsername() {
		return getTopStringFromFile("usernames");
	}

	public static String getEmailPrefix() {
		return getTopStringFromFile("emails");
	}

	public static String getLoginDetails() {
		return getTopStringFromFile("premade_accounts");
	}

	private static String getTopStringFromFile(String filename) {
		String result = null;

		File file = new File(TBot.getBot().getScriptHandler().getScript().getStorageDirectory() + "\\" + filename + ".txt");
		if(!file.exists()) {
			try {
				LogHandler.log("No usernames found. Please fill " + filename + ".txt with usernames.",Level.SEVERE);
				LogHandler.log(filename + ".txt can be found in your script storage directory.",Level.SEVERE);
				file.createNewFile();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			List<String> stringStorage = new LinkedList<>();
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = reader.readLine();
			while(line != null) {
				stringStorage.add(line);
				line = reader.readLine();
			}
			reader.close();

			result = stringStorage.get(0);
			stringStorage.remove(0);

			PrintWriter writer = new PrintWriter(file);
			for(String s : stringStorage) {
				writer.println(s);
			}
			writer.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
