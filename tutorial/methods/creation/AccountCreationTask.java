package scripts.tutorial.methods.creation;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.zip.GZIPInputStream;

public class AccountCreationTask {

	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36";
	private static final String JAGEX_URL = "https://secure.runescape.com/m=account-creation/g=oldscape/create_account_funnel.ws";
	private final String displayName;
	private final String email;
	private final String pass;
	private final int age;

	public AccountCreationTask(String displayName, String emailAddress, String password, int age) throws UnsupportedEncodingException {
		this.displayName = displayName;
		this.email = emailAddress;
		this.pass = password;
		this.age = age;
	}

	/**
	 * @return success opcode, -1 = unknown, 0 = success, 1 = name already taken, 2 = mail already taken, 3 = bad words in name
	 * any other opcode = server error, 403, 404 etc
	 * @throws java.io.IOException
	 */
	public int execute() throws IOException {
		URL url = new URL(JAGEX_URL);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		con.setRequestProperty("Host", "secure.runescape.com");
		//con.setRequestProperty("Origin", "https://secure.runescape.com");
		//con.setRequestProperty("Upgrade-Insecure-Requests", "1");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US;en;q=0.8");
		con.setRequestProperty("Accept-Encoding", "gzip, deflate");
		con.setRequestProperty("Connection", "keep-alive");
		con.setRequestProperty("Referer", "https://secure.runescape.com/m=account-creation/g=oldscape/create_account_funnel.ws");

		String POSTDATA = "onlyOneEmail=1&age=" + age +
				"&displayname_present=true&displayname=" + this.displayName +
				"&email1=" + URLEncoder.encode(email, "UTF-8") +
				"&password1=" + pass + "&password2=" + pass +
				"&agree_email=on&agree_pp_and_tac=1&submit=Join+Now";
		// POSTDATA = URLEncoder.encode(POSTDATA, "UTF-8");
		con.setRequestProperty("Content-Length", "" + POSTDATA.length());
		final DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(POSTDATA);
		wr.flush();
		wr.close();

		final int responseCode = con.getResponseCode();
		if (responseCode != 200) {
			System.out.println("RESPONSE CODE: " + responseCode);
			return responseCode;
		}
		InputStream is;
		try {
			is = new GZIPInputStream(con.getInputStream());
		} catch (Exception e) {
			is = con.getInputStream();
		}

		int result = -1;
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		try {
			String response = br.readLine();
			while (response != null) {

				if (response.contains("Sorry, that character name is not available.<")) {
					result = 1;
					break;
				} else if (response.contains("Your email address has already been taken. Please choose a different address.")
						&& !response.contains(":")) {
					result = 2;
					break;
				} else if (response.contains("Your chosen character name contains bad words.<")) {
					result =  3;
					break;
				} else if (response.contains("You have been blocked")) {
					result = 4;
					break;
				} else if (response.contains("Unfortunately you are not eligible")) {
					result = 5;
					break;
				} else if (response.contains("Not received an email?")) {
					result =  0;
					break;
				}

				response = br.readLine();
			}
		} finally {
			try {
				System.out.println(" ----- CLOSE -----");
				br.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public String toString() {
		return "AccountCreationTask{" +
				"displayName='" + displayName + '\'' +
				", email='" + email + '\'' +
				", pass='" + pass + '\'' +
				", age=" + age +
				'}';
	}

	public String getEmail() {
		return email;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getPass() {
		return pass;
	}
}