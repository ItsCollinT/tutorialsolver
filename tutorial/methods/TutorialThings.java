package scripts.tutorial.methods;

import api.CNPCChat;
import org.tbot.methods.*;
import org.tbot.wrappers.WidgetChild;
import scripts.tutorial.methods.tactions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class TutorialThings {

	/*
	BUGS
	Tutorial Rat Pit - Arrows - Doesnt go to ladder if its too far away, add manual fix, walk to gate
	 */

	private static ArrayList<TAction> actions = new ArrayList<TAction>(Arrays.asList(
			new DesignLooks(), new UseItemsAction(), new ClickArrowAction(), new FlashingTab()
	));

	public static int complete() {
		if(settingActionMap.isEmpty()) {
			initMap();
		}

		WidgetChild wc = Widgets.getWidget(548, 120);
		if(wc != null && wc.isVisible()) {
			wc.click();
			out("Click Widget A");
			return Random.nextInt(450, 650);
		}

		WidgetChild wc2 = Widgets.getWidget(162,33);
		if(wc2 != null && wc2.isVisible()) {
			wc2.click();
			out("Click Widget B");
			return Random.nextInt(450, 650);
		}

		if(Bank.isOpen()) {
			Bank.close();
			out("Close Bank");
			return Random.nextInt(450, 650);
		}

		if(chats.perform()) {
			out("Perform Chat");
			return Random.nextInt(450,650);
		} else if(CNPCChat.isChatOpen()) {
			if(NPCChat.isChatOptionsOpen()) {
				processExtraChat();
				return Random.nextInt(450, 650);
			}
			out("Perform Chat");
			org.tbot.methods.input.keyboard.Keyboard.pressKey(' ');
			return Random.nextInt(450,650);
		}

		if(Players.getLocal().getAnimation() != -1) {
			out("Animating...");
			return Random.nextInt(450, 650);
		}

		if(ManualActions.isException()) {
			out("> Manual Action");
			return ManualActions.complete();
		}

		for(TAction a : actions) {
			if(a.canComplete()) {
				out("TAction");
				return a.complete();
			}
		}

		return ManualActions.complete();
	}

	private static void out(String s) {
		System.out.println("[TUT] " + s);
	}

	private static void initMap() {
		settingActionMap.put(10,"Open");
		settingActionMap.put(40,"Chop down");
	}

	private static void processExtraChat() {
		switch (Settings.get(281)) {
			case 0: {
				if(NPCChat.getOptionCount() == 3) {
					NPCChat.selectOptionExactKeyboard(GUIDE_CHAT_OPTIONS[Random.nextInt(GUIDE_CHAT_OPTIONS.length)]);
				}
				break;
			}
		}
	}

	public static HashMap<Integer,String> settingActionMap = new HashMap<Integer,String>();

	private static CNPCChat chats = new CNPCChat("Yes.");

	private static String[] GUIDE_CHAT_OPTIONS = {
			"I am brand new! This is my first time here.",
			"I am brand new! This is my first time here.",
			"I am brand new! This is my first time here.",
			"I am brand new! This is my first time here.",
			"I've played in the past, but not recently.",
			"I've played in the past, but not recently.",
			"I've played in the past, but not recently.",
			"I am an experienced player."
	};
}
