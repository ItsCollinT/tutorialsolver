package scripts.tutorial.methods;

import api.CWalking;
import org.tbot.methods.*;
import org.tbot.methods.combat.magic.Magic;
import org.tbot.methods.combat.magic.SpellBooks;
import org.tbot.methods.tabs.Equipment;
import org.tbot.methods.tabs.Inventory;
import org.tbot.methods.walking.Walking;
import org.tbot.util.Condition;
import org.tbot.util.Evaluator;
import org.tbot.util.Filter;
import org.tbot.wrappers.*;

import java.util.ArrayList;
import java.util.Arrays;

public class ManualActions {

	public static boolean isException() {
		if(Settings.get(281) == 500 && Players.getLocal().getLocation().getY() < 9515) {
			return true;
		}
		return exceptionList.contains(Settings.get(281));
	}

	public static int complete() {
		switch(Settings.get(281)) {
			case 0: {

			} break;

			case 3: {
				Widgets.openTab(Widgets.TAB_OPTIONS);
			} break;

			case 30: {
				Widgets.openTab(Widgets.TAB_INVENTORY);
			} break;

			case 60: {
				Widgets.openTab(Widgets.TAB_STATS);
			} break;

			case 90: {
				if(Inventory.getCount(SHRIMP_RAW) < 2) {
					NPC fish = Npcs.getNearest("Fishing spot");
					if(fish != null) {
						fish.interact("Net");
					}
				} else if(Players.getLocal().getAnimation() == -1) {
					if(!useItemOnObject(SHRIMP_RAW,"Fire")) {
						Inventory.deselectItem();
						if(!Inventory.contains(2511)) {
							GameObject tree = GameObjects.getNearest("Tree");
							if(tree != null && tree.isOnScreen()) {
								tree.interact("Chop down");
							}
						}
					}
				}
			} break;

			case 100: {
				if(Players.getLocal().getAnimation() == -1) {
					if(!useItemOnObject(SHRIMP_RAW,"Fire")) {
						Inventory.deselectItem();
						if(!Inventory.contains(2511)) {
							GameObject tree = GameObjects.getNearest("Tree");
							if(tree != null && tree.isOnScreen()) {
								tree.interact("Chop down");
							}
						}
					}
				}
			} break;

			case 110: {
				if(Players.getLocal().getAnimation() == -1) {
					if(!useItemOnObject(SHRIMP_RAW,"Fire")) {
						Inventory.deselectItem();
						if(!Inventory.contains(2511)) {
							GameObject tree = GameObjects.getNearest("Tree");
							if(tree != null && tree.isOnScreen()) {
								tree.interact("Chop down");
							}
						}
					}
				}
			} break;

			case 160: {
				useItemOnObject(DOUGH,"Range");
			} break;

			case 170: {
				Widgets.openTab(Widgets.TAB_MUSIC);
			} break;

			case 183: {
				Widgets.openTab(Widgets.TAB_EMOTES);
			} break;

			case 190: {
				Widgets.openTab(Widgets.TAB_OPTIONS);
			} break;

			case 187: {
				if(Widgets.isTabOpen(Widgets.TAB_EMOTES)) {
					WidgetChild wc = Widgets.getWidget(216,1);
					if(wc != null && wc.isVisible()) {
						wc = wc.getChild(Random.nextInt(19));
						if(wc != null && wc.isVisible()) {
							wc.click();
						}
					}
				} else {
					Widgets.openTab(Widgets.TAB_EMOTES);
				}
			} break;

			case 200: {
				Walking.setRun(true);
			} break;

			case 230: {
				Widgets.openTab(Widgets.TAB_QUESTS);
			} break;

			case 260: {
				CWalking.getCWalking().traverse(new Tile(3082,9504,0), true, true);
			} break;

			case 270: {
				if(!Players.getLocal().isMoving()) {
					doRock("Prospect");
				}
			} break;

			case 280: {
				if(!Players.getLocal().isMoving()) {
					doRock("Prospect");
				}
			} break;

			case 300: {
				if(!Players.getLocal().isMoving()) {
					doRock("Mine");
				}
			} break;

			case 310: {
				if(!Players.getLocal().isMoving()) {
					doRock("Mine");
				}
			} break;

			case 320: {
				if(!Inventory.contains(ORE[1])) {
					doRock("Mine",rock1ID);
				} else if(!Inventory.contains(ORE[0])) {
					doRock("Mine",rock2ID);
				} else if(!Players.getLocal().isMoving()) {
					useItemOnObject(ORE[Random.nextInt(1)],"Furnace");
				}
			} break;

			case 350: {
				if(!clickWidget(312, 2)) {
					doObjectAt(new Tile(3083,9499,0));
					Time.sleepUntil(new Condition() {
						@Override
						public boolean check() {
							return clickWidget(312, 2);
						}
					}, Random.nextInt(2350,3150));
				}
			} break;

			case 370: {
				NPC go = Npcs.getNearest("Combat Instructor");
				if(go != null) {
					if(go.isOnScreen()) {
						go.interact("");
					} else if(!CWalking.getCWalking().canReach(go.getLocation())) {
						Walking.walkTileMM(go.getLocation().getRandomized(1));
					} else {
						CWalking.getCWalking().traverseDoors(go.getLocation(),true);
					}
				}
			} break;

			case 390: {
				Widgets.openTab(Widgets.TAB_EQUIPMENT);
			} break;

			case 400: {
				if(!clickWidget(387,17)) {
					Widgets.openTab(Widgets.TAB_EQUIPMENT);
				}
			} break;

			case 405: {
				WidgetChild equips = Widgets.getWidget(85,0);
				for(WidgetChild wc : equips.getChildren()) {
					if(wc.getItemID() == DAGGER) {
						wc.click();
						break;
					}
				}
			} break;

			case 420: {
				Item clicky = Inventory.getFirst(DAGGER);
				if(clicky != null) {
					clicky.click();
				}
			} break;

			case 460: {
				if(Players.getLocal().getInteractingEntity() == null) {
					NPC go = Npcs.getNearest("Giant rat");
					if(go != null) {
						if(go.isOnScreen()) {
							go.interact("Attack");
						} else if(!CWalking.getCWalking().canReach(go.getLocation())) {
							Walking.walkTileMM(go.getLocation().getRandomized(1));
						} else {
							CWalking.getCWalking().traverseDoors(go.getLocation(),true);
						}
					}
				}
			} break;

			case 480:
			case 490: {
				if(Equipment.containsAll(841,882)) {
					if(Players.getLocal().getInteractingEntity() == null) {
						NPC go = Npcs.getNearest("Giant rat");
						if(go != null) {
							if(go.isOnScreen()) {
								go.interact("");
							} else {
								Walking.walkTileMM(go.getLocation().getRandomized(1));
							}
						}
					}
				} else {
					Item a = Inventory.getItemClosestToMouse(841,882);
					if(a != null) {
						a.click();
						return Random.nextInt(450, 650);
					}
				}
			} break;

			case 500: {
				Walking.walkTileMM(new Tile(3112,9518,0).getRandomized(2));
			} break;

			case 525: {
				if(Players.getLocal().getLocation().equals(new Tile(3120,3121,0))) {
					Walking.walkTileMM(new Tile(3123,3123,0).getRandomized(2));
				} else {
					exceptionList.remove(new Integer(525));
				}
			} break;

			case 550: {
				CWalking.getCWalking().traverse(new Tile(3130,3109,0), true, true);
			} break;

			case 560: {
				Widgets.openTab(Widgets.TAB_PRAYER);
			} break;

			case 580: {
				Widgets.openTab(Widgets.TAB_FRIENDS);
			} break;

			case 590: {
				Widgets.openTab(Widgets.TAB_IGNORE);
			} break;

			case 620: {
				CWalking.getCWalking().traverse(new Tile(3140,3087,0), true, true);
			} break;

			case 630: {
				Widgets.openTab(Widgets.TAB_MAGIC);
			} break;

			case 650: {
				if(Players.getLocal().getLocation().getY() == 3091
						&& Players.getLocal().getLocation().getX() > 3137
						&& Players.getLocal().getLocation().getX() < 3141) {
					if(Magic.hasSpellSelected()) {
						NPC chicken = Npcs.getNearest(new Filter<NPC>() {
							@Override
							public boolean accept(NPC npc) {
								return npc.getName().equals("Chicken")
										&& !npc.isHealthBarVisible()
										&& npc.getLocation().getY() < 3094
										&& npc.getLocation().getX() < 3141;
							}
						});

						if(chicken != null) {
							if(chicken.isOnScreen()) {
								chicken.interact("Cast Wind Strike -> Chicken");
								return Random.nextInt(950,1350);
							}
						}
					} else {
						Widgets.openTab(Widgets.TAB_MAGIC);
						Magic.cast(SpellBooks.Modern.WIND_STRIKE);
					}
				} else {
					Magic.deselectSpell();
					CWalking.getCWalking().traverse(new Tile(3138 + Random.nextInt(2),3091), true, true);
				}
			} break;

			case 1000: return -1;

			default: {
				System.out.println("No Manual Action Defined For: " + Settings.get(281));
			}
		}

		return Random.nextInt(450, 650);
	}

	private static boolean useItemOnObject(int itemID, final String objectName) {
		if(!Inventory.hasItemSelected()) {
			Item shrimp = Inventory.getItemClosestToMouse(itemID);
			if(shrimp != null) {
				shrimp.click();
			}
		} else {
			GameObject fire = GameObjects.getNearest(new Filter<GameObject>() {
				@Override
				public boolean accept(GameObject gameObject) {
					return gameObject.getName().equals(objectName)
							&& (!objectName.equals("Furnace") || gameObject.hasAction("Use"));
				}
			});
			if(fire != null) {
				if(fire.isOnScreen()) {
					fire.interact("Use " + Inventory.getLastSelectedItemName() + " -> " + objectName);
				} else {
					Walking.walkTileMM(fire.getLocation().getRandomized(1));
				}
			} else {
				System.out.println("Object [" + objectName + "] is null.");
				return false;
			}
		}
		return true;
	}

	private static boolean clickWidget(int a, int b) {
		WidgetChild wc = Widgets.getWidget(a,b);
		if(wc != null && wc.isVisible()) {
			wc.click();
			return true;
		}
		return false;
	}

	private static void doObjectAt(Tile t) {
		if(t != null) {
			if (CWalking.getCWalking().traverseDoors(t, true)) {
				if (t.isOnScreen()) {
					GameObject clickOnTile = GameObjects.getTopAt(t);
					if (clickOnTile != null) {
						if (TutorialThings.settingActionMap.containsKey(Settings.get(281))) {
							clickOnTile.interact(TutorialThings.settingActionMap.get(Settings.get(281)));
						} else {
							clickOnTile.interact("");
						}
					} else {
						if (TutorialThings.settingActionMap.containsKey(Settings.get(281))) {
							t.interact(TutorialThings.settingActionMap.get(Settings.get(281)));
						} else {
							t.interact("");
						}
					}
				} else if (!CWalking.getCWalking().canReach(t)) {
					Walking.walkTileMM(t.getRandomized(1));
				}
			}
		}
	}

	private static void doRock(String action) {
		if(Game.getHintArrow() != null && Game.getHintArrow().getLocation() != null) {
			GameObject rock = GameObjects.getTopAt(Game.getHintArrow().getLocation());
			if(rock != null) {
				if(rock.isOnScreen()) {
					final int rockID = rock.getID();
					if(rock1ID == -1) {
						rock1ID = rockID;
					} else if(rock2ID == -1 && rock1ID != rockID) {
						rock2ID = rockID;
					}
					GameObject nextRock = GameObjects.getBest(new Evaluator<GameObject>() {
						@Override
						public double evaluate(GameObject gameObject) {
							return gameObject.distance() + Random.nextInt(1);
						}
					}, new Filter<GameObject>() {
						@Override
						public boolean accept(GameObject gameObject) {
							return gameObject.getID() == rockID;
						}
					});
					if (nextRock != null) {
						nextRock.interact(action);
					}
				} else if(Random.nextInt(0,3) == 0) {
					Camera.turnTo(rock);
				} else {
					Walking.walkTileMM(rock.getLocation().getRandomized(1));
				}
			}
		}
	}

	private static void doRock(String action, final int id) {
		GameObject nextRock = GameObjects.getBest(new Evaluator<GameObject>() {
			@Override
			public double evaluate(GameObject gameObject) {
				return gameObject.distance() + Random.nextInt(1);
			}
		}, new Filter<GameObject>() {
			@Override
			public boolean accept(GameObject gameObject) {
				return gameObject.getID() == id;
			}
		});
		if (nextRock != null) {
			nextRock.interact(action);
		}
	}

	private static int rock1ID = -1, rock2ID = -1;
	private static final int SHRIMP_RAW = 2514;
	private static final int DAGGER = 1205;
	private static final int DOUGH = 2307;
	private static final int[] ORE = new int[] {436,438};
	private static ArrayList<Integer> exceptionList = new ArrayList<Integer>(
			Arrays.asList(
					3, 160, 270, 280, 300, 310, 320, 370, 460, 480, 490, 525, 650
			)
	);
}
