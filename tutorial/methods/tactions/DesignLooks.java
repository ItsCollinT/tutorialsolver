package scripts.tutorial.methods.tactions;

import org.tbot.methods.Random;
import org.tbot.methods.Widgets;
import org.tbot.wrappers.WidgetChild;

public class DesignLooks extends TAction {

	@Override
	public boolean canComplete() {
		WidgetChild lookWidget = Widgets.getWidget(269, 97);
		return lookWidget != null && lookWidget.isVisible();
	}

	@Override
	public int complete() {
		if(Random.nextInt(0, 11) <= 4) {
			WidgetChild femaleWidget = Widgets.getWidget(appearanceWidget,137);
			if(femaleWidget != null && femaleWidget.isVisible()) {
				femaleWidget.click();
			}
		}

		int randomChanges = Random.nextInt(5,25);
		WidgetChild changeApperance = null;
		for(int i = 0; i < randomChanges; i++) {
			changeApperance = Widgets.getWidget(appearanceWidget,APPEARANCE_WIDGETS[Random.nextInt(APPEARANCE_WIDGETS.length)]);
			if(changeApperance != null && changeApperance.isVisible()) {
				changeApperance.click();
			}
		}

		WidgetChild accept = Widgets.getWidget(appearanceWidget,99);
		if(accept != null && accept.isVisible()) {
			accept.click();
		}

		return Random.nextInt(450,750);
	}

	private int appearanceWidget = 269;
	private final int[] APPEARANCE_WIDGETS = new int[] {105,106,107,108,109,110,111,112,113,114,
			115,116,117,118,119,121,122,123,124,125,127,129,130,131};
}
