package scripts.tutorial.methods.tactions;

import api.CWalking;
import org.tbot.methods.*;
import org.tbot.methods.tabs.Inventory;
import org.tbot.methods.walking.Walking;
import org.tbot.util.Condition;
import org.tbot.wrappers.GameObject;
import org.tbot.wrappers.Tile;
import scripts.tutorial.methods.TutorialThings;

public class ClickArrowAction extends TAction {

	@Override
	public boolean canComplete() {
		return Game.getHintArrow() != null && Game.getHintArrow().getLocation() != null;
	}

	@Override
	public int complete() {
		Inventory.deselectItem();

		if(Players.getLocal().isMoving()) {
			Time.sleepUntil(new Condition() {
				@Override
				public boolean check() {
					return !Players.getLocal().isMoving();
				}
			}, Random.nextInt(750,1630));
		}

		if(Game.getHintArrow() == null) {
			return Random.nextInt(450,650);
		}

		Tile t = Game.getHintArrow().getLocation();
		if(t != null) {
			if(CWalking.getCWalking().traverseDoors(t, true)) {
				if (t.isOnScreen()) {
					GameObject clickOnTile = GameObjects.getTopAt(t);
					if (clickOnTile != null) {
						if (TutorialThings.settingActionMap.containsKey(Settings.get(281))) {
							clickOnTile.interact(TutorialThings.settingActionMap.get(Settings.get(281)));
						} else {
							System.out.println("(Interact:" + Settings.get(281) + ")");
							clickOnTile.interact("");
						}
					} else {
						if (TutorialThings.settingActionMap.containsKey(Settings.get(281))) {
							t.interact(TutorialThings.settingActionMap.get(Settings.get(281)));
						} else {
							System.out.println("(InteractTile:" + Settings.get(281) + ")");
							t.interact("");
						}
					}
					return Random.nextInt(650,1530);
				} else if (!CWalking.getCWalking().canReach(t)) {
					if(t.isOnMiniMap()) {
						Walking.walkTileMM(t.getRandomized(1));
					} else {
						if(t.getRandomized(5).isOnMiniMap()) {
							Walking.walkTileMM(t.getRandomized(1));
						}
					}
				}
			}
		}
		return Random.nextInt(450, 650);
	}
}
