package scripts.tutorial.methods.tactions;

import org.tbot.methods.GameObjects;
import org.tbot.methods.Players;
import org.tbot.methods.Random;
import org.tbot.methods.Settings;
import org.tbot.methods.tabs.Inventory;
import org.tbot.methods.walking.Walking;
import org.tbot.wrappers.GameObject;
import org.tbot.wrappers.Item;

import java.util.ArrayList;
import java.util.Arrays;

public class UseItemsAction extends TAction {

	private ArrayList<Integer> useItems = new ArrayList<Integer>(Arrays.asList(
			590,2511, //tinderbox + log
			2516,1929, //flour + water
			1277,1171, //sword + shield
			882,841 //bow + arrows?
	));

	@Override
	public boolean canComplete() {
		for(int i : Inventory.getInventory()) {
			for(Integer b : useItems) {
				if(i == b) {
					if(i != 590 || Inventory.contains(2511)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public int complete() {
		removeItems();

		if(moveFromFire) {
			GameObject fire = GameObjects.getTopAt(Players.getLocal().getLocation());
			if(fire != null && fire.getName().startsWith("Fi")) {
				Walking.walkTileMM(Players.getLocal().getLocation(),3,3);
				return Random.nextInt(850,1530);
			}
		}

		for(Integer i : useItems) {
			Item a = Inventory.getItemClosestToMouse(i);

			if(a != null) {
				if(!a.getName().equals(Inventory.getLastSelectedItemName())) {
					a.click();
					return Random.nextInt(450, 650);
				}
			}
		}

		return Random.nextInt(450, 650);
	}

	private void removeItems() {
		int setting = Settings.get(281);

		if(setting > 110) {
			useItems.remove(new Integer(590));
			useItems.remove(new Integer(2511));
			moveFromFire = false;
		}

		if(setting > 470) {
			useItems.remove(new Integer(1277));
			useItems.remove(new Integer(1171));
			moveFromFire = false;
		}

		if(setting > 500) {
			useItems.remove(new Integer(882));
			useItems.remove(new Integer(841));
			moveFromFire = false;
		}
	}

	private boolean moveFromFire = true;
}
