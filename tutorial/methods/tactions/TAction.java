package scripts.tutorial.methods.tactions;

public abstract class TAction {

	public abstract boolean canComplete();
	public abstract int complete();

}
