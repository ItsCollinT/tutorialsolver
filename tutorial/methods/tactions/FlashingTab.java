package scripts.tutorial.methods.tactions;

import org.tbot.methods.Game;
import org.tbot.methods.Random;
import org.tbot.methods.Widgets;
import org.tbot.wrappers.WidgetChild;

public class FlashingTab extends TAction {

	@Override
	public boolean canComplete() {
		WidgetChild logout = Widgets.getWidget(548,41);
		if(logout != null && !logout.isVisible()) {
			return false;
		}

		if(startup && Game.isLoggedIn()) {
			for(Tab t : tabs) {
				WidgetChild wc = Widgets.getWidget(548, t.getWidget());
				if(wc != null) {
					t.setVisible(wc.isVisible());
				}
			}
			startup = false;
		}

		for(Tab t : tabs) {
			WidgetChild wc = Widgets.getWidget(548, t.getWidget());
			if(wc != null && wc.isVisible() != t.isVisible()) {
				toOpen = wc;
				toOpenTab = t;
				return true;
			}
		}
		toOpen = null;
		toOpenTab = null;
		return false;
	}

	@Override
	public int complete() {
		if(toOpen != null) {
			toOpen.click();
			toOpenTab.setVisible(toOpen.isVisible());
			System.out.println(toOpen.getIndex() + ":" + toOpenTab.getWidget() + ":"
					+ toOpenTab.isVisible() + ":" + toOpen.isVisible());
		}
		return Random.nextInt(450, 650);
	}

	private boolean startup = true;
	private WidgetChild toOpen = null;
	private Tab toOpenTab = null;
	private Tab[] tabs = new Tab[] {
			new Tab(49,true),
			new Tab(50,true),
			new Tab(51,true),
			new Tab(52,true),
			new Tab(53,true),
			new Tab(54,true),
			new Tab(55,true),

			new Tab(32,true),
			new Tab(33,true),
			new Tab(34,true),
			new Tab(36,true),
			new Tab(37,true),
			new Tab(38,true)
	};

	class Tab {

		Tab(int widget, boolean visible) {
			this.widget = widget;
			this.visible = visible;
		}

		public int getWidget() {
			return widget;
		}

		public boolean isVisible() {
			return visible;
		}

		public void setVisible(boolean visible) {
			this.visible = visible;
		}

		private int widget = 0;
		private boolean visible = false;
	}
}
