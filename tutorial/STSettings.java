package scripts.tutorial;

import org.tbot.methods.Random;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class STSettings {

	// *** Config File Names *** //
	public static final String CREATION_SETTINGS = "creation_settings";
	public static final String GENERAL_SETTINGS = "general_settings";
	public static final String GAME_SETTINGS = "game_settings";
	private static final String[] propFiles = new String[] {CREATION_SETTINGS,GENERAL_SETTINGS,GAME_SETTINGS};

	// *** Script Settings *** //
	public static boolean terminateTopbot = true;
	public static int appearanceChanges = 0;

	// *** Paint Info *** //
	public static String ipAddress = "";
	public static String accEmail = "";

	public static boolean createPropertyFile(String s) {
		Properties result = new Properties();

		if(s.equals(CREATION_SETTINGS)) {
			result.setProperty("minAge","16");
			result.setProperty("maxAge","25");
			result.setProperty("baseEmail","ex?????@hotmail.com");
			result.setProperty("staticName","ts?????");
			result.setProperty("nameType","1"); //0 = WORD LIST, 1 = STATIC, 2 = HARVEST
			result.setProperty("emailType","1"); //0 = WORD LIST, 1 = STATIC
			result.setProperty("defaultDomain","@hotmail.com");
			result.setProperty("passMinLen","6");
			result.setProperty("passMaxLen","12");
			result.setProperty("staticPass","DefaultPass");
			result.setProperty("useStatic","false");
			result.setProperty("useAlpha","true");
			result.setProperty("useDigits","true");
			result.setProperty("useCaps","true");
			return SellTutorial.propsManager.savePropertiesFile(s,result);
		}

		if(s.equals(GENERAL_SETTINGS)) {
			result.setProperty("exitTopbot","false");
			result.setProperty("minAppearanceChanges","5");
			result.setProperty("maxAppearanceChanges","50");
			result.setProperty("createAccounts","true");
			result.setProperty("walkTo","3233,3230,0");
			result.setProperty("bankItemsFirst","false");
			result.setProperty("dontBankIDs","0");
			return SellTutorial.propsManager.savePropertiesFile(s,result);
		}

		if(s.equals(GAME_SETTINGS)) {
			result.setProperty("zoomOut","true");
			result.setProperty("musicVolume","0");
			result.setProperty("soundVolume","0");
			result.setProperty("areaVolume","0");
			result.setProperty("brightness","2");
			result.setProperty("clickType","0");
			return SellTutorial.propsManager.savePropertiesFile(s,result);
		}

		return false;
	}

	public static void loadScriptSettings() {
		Properties scriptProps = SellTutorial.propsManager.getPropertiesFile(GENERAL_SETTINGS);
		terminateTopbot = Boolean.parseBoolean(scriptProps.getProperty("exitTopbot"));
		appearanceChanges = Random.nextInt(Integer.parseInt(scriptProps.getProperty("minAppearanceChanges")),
				Integer.parseInt(scriptProps.getProperty("maxAppearanceChanges")));
	}

	public static String getPropertyFromPropsFile(String name, Properties prop) {
		String result = prop.getProperty(name);
		if(result == null) {
			result = getDefaultValue(name);
			prop.setProperty(name,result);
		}
		return result;
	}

	public static void fillDefaultValues() {
		defaultValues.put("minAge", "16");
		defaultValues.put("maxAge", "25");
		defaultValues.put("baseEmail","ex?????@hotmail.com");
		defaultValues.put("staticName", "ts?????");
		defaultValues.put("nameType", "1"); //0 = WORD LIST, 1 = STATIC, 2 = HARVEST
		defaultValues.put("emailType", "1"); //0 = WORD LIST, 1 = STATIC
		defaultValues.put("defaultDomain", "@hotmail.com");
		defaultValues.put("passMinLen", "6");
		defaultValues.put("passMaxLen", "12");
		defaultValues.put("staticPass", "DefaultPass");
		defaultValues.put("useStatic", "false");
		defaultValues.put("useAlpha", "true");
		defaultValues.put("useDigits", "true");
		defaultValues.put("useCaps", "true");
		defaultValues.put("exitTopbot", "false");
		defaultValues.put("minAppearanceChanges", "5");
		defaultValues.put("maxAppearanceChanges", "50");
		defaultValues.put("createAccounts","true");
		defaultValues.put("walkTo","3233,3230,0");
		defaultValues.put("bankItemsFirst","false");
		defaultValues.put("dontBankIDs","0");
	}

	public static String getDefaultValue(String propertyName) {
		return defaultValues.get(propertyName);
	}

	private static Map<String,String> defaultValues = new HashMap<>();
}